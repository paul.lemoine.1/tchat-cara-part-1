# Le projet à réaliser: 
	Le projet consiste à la réalisation via le framework Griffon d'un systeme de tchat entre utilisateurs.
	Les parties se décomposent de la manière suivante :
		1er Partie : 
			-Configuration d'Eclipse et réalisation de votre première vue.
		2nd Partie : 
			-Création du MVC de tchat 
			-Création du service associé
		3eme Partie : 
			-Création et intégration du MVC Ip pour la connection via IP
			
# 1. Partie Configuration : 

	Dans cette première partie vous apprendrez à configurer Eclipse pour un projet Griffon/Groovy/GroovyFx 
	ainsi que la création de votre première vue qui consiste à un simple menu permettant de rentrer son pseudo, 
	de le valider et d'un bouton pour quitter l'application.

	Ce que vous devez obtenir à la fin de la première partie :
	
![alt text](https://imageshack.com/a/img924/451/ebbbfk.png "Resultat partie 1")
	
## 1.a Configuration d'Eclipse :
Avant de cloner le projet Griffon et pour éviter une mort douloureuse de notre cher IDE Eclipse, il faut :
a. Télécharger le Groovy Development Tools:
 - Ouvrir Eclipse, allez dans le menu "help", puis ouvrir le MarketPlace d'Eclipse :
 
 ![alt text](https://imageshack.com/a/img923/5240/VO1lWc.png "Menu help")
 
 - Faite une recherche dans le MarketPlace avec le mot Groovy et téléchargez le Groovy Develop
 ![alt text](https://imageshack.com/a/img923/7776/wjTusI.png "Market Place")
- Lors de l'installation cochez l'ensemble des propositions que l'installateur Groovy vous propose (Maven et le compiler).


- Une fois l'ensemble de ses étapes faites, Eclipse est "configuré" pour rester zen en toutes circonstances.
## 1.b Clone et configuration du projet et on est parti !

 - Tout d'abord cloner le projet dans votre répertoire favori.
	 - Le lien est : https://gitlab.com/paul.lemoine.1/tchat-cara-part-1.git
 - Une fois le projet cloner, il faut l'ouvrir dans Eclipse via l'import de projet existant Maven.
 - Par la suite faite un clic droit dessus et allez dans les propriétés :
 
![alt text](https://imageshack.com/a/img921/5264/sWjHNC.png "Properties")
- Allez dans java Build path, puis dans Libraries :
![alt text](https://imageshack.com/a/img923/3821/BLsfrI.png "library")
- Sélectionnez JRE System puis Access Rules et cliquez sur Edit

- Donnez l'accès à Eclipse d'utiliser les librairies JavaFx
	-   Cliquez sur add, mettre la résolution en Accessible puis écrire dans al rule pattern : javafx/**
	
	![alt text](https://imageshack.com/a/img923/5526/23suVF.png "library")
	- Validez, appliquez et fermez

## 1.c Création des configurations maven :

- Avant de lancer le projet, Nous allons créer deux configurations maven :
	-  Clic droit sur le projet et choisir Run as, puis Maven Build...
	- Pour compiler le projet : 
		- Le goal doit être : clean install -Dcheckstyle.skip
		- Cochez Skip Tests
		- puis apply et close
		
		![alt text](https://imageshack.com/a/img924/388/LsVBIB.png "conf Maven")
	- Pour lancer le projet (refaire une configuration):
		- Le goal doit être : -Prun
		- puis apply et close
		
# 2. Quelques liens bien utiles : 
L'intégration sous Eclipse étant un peu complexe, sur certaines parties du projet l'ensemble des objets manipulés seront de type Object, rien ne sert de matraquer vos touches ctrl + space ! L'auto-complétion n'est pas présentes, pour vous aider voici quelques liens vous permettant d'appréhender ce qui suit : 
	
    - http://griffon-framework.org/guide/2.15.0/
	
    - http://groovyfx.org/docs/index.html
    
    - https://github.com/griffon/griffon/blob/development/samples/sample-javafx-groovy/griffon-app/views/sample/javafx/groovy/SampleView.groovy	
- Comme vous pouvez l'observer, un projet griffon se découpe en plusieurs packages :
	- Le package conf :
		- Permet la déclarations des MVC
	- Le package controller :
		- Permet de gérer les interactions avec la vue et les mises à jours du model
	- Le package Model :
		- permet le stockage d'informations
	- Le package View :
		- permet la déclaration des vues de notre projet

## 2.a Création de la vue :
- le résultat à obtenir est : 
	![alt text](https://imageshack.com/a/img924/451/ebbbfk.png "Resultat partie 1")

- Pour se faire, placez vous dans la classe AppView :
	- Notre vue repose sur l'utilisation de GroovyFx lui même 
	- Puis dans la méthode initUI():
	-  Notre vue se compose de la manière suivante :
		-  le builder
			- le stage : 
				- id: stageMenu
				- titre : Tchat
			- la scene : 
				- id: scene 
			- gridPane: 
				- id: gridMenu
				- label
					- text: 'Pseudo'
				- textField: 
					- id: fieldPseudo
				- button : 
				    - id : Valider 
				- button : 
				    - id : Quitter 

-Exemple de code :
	
    stage(id:'stage', title: 'test', visible: true) {
        scene(id: 'scene', fill: WHITE, width: 200, height: 200) {
            label(text: 'Je suis un label:')
            button(prefWidth: 200, id: 'test')
        }
    }
    
- Une fois notre vue créée nous allons passer au controller et à sa liaison avec la vue.

## 2.b Partie Controller :

- Dans cette partie nous vous demandons pour le moment de coder la fonction permettant de quitter l'application.
	- Cette fonctionnalité utilise la méthode quit() et se base sur l'application générale instanciée par Griffon.
	-  Vous trouverez quelques informations concernant l'accès à l'objet application dans le Griffon Guide.

- Une fois votre méthode réalisée, il vous faut associer votre fonction à votre vue.

	- Pour se faire, il faut mettre en place une action sur le bouton de fermeture.
	- Puis associer votre contenant, ici notre GridPane, à notre controller via une méthode permettant d'associer des actions.

## 2.c Partie Model :
- Dans cette partie nous allons juste définir ce que doit stocker la vue, ici nous n'avons que le pseudo de l'utilisateur.
	- Le pseudo est de type string et se déclare en tant qu'un observable de JavaFx.
	
-  Une fois notre pseudo déclaré il faut le "binder" sur la valeur du textField.
	- Pour se faire, retournez sur la vue et paramétrez le textField.


## 3. Lancement de l'application

- Une fois l'ensemble de ses méthodes réalisées, vous pouvez lancer votre application  via la configuration Maven -Prun.
