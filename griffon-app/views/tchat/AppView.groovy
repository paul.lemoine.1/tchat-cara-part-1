package tchat

import griffon.core.artifact.GriffonView
import griffon.inject.MVCMember
import griffon.metadata.ArtifactProviderFor
import javafx.scene.control.TabPane
import org.codehaus.griffon.runtime.javafx.artifact.AbstractJavaFXGriffonView
import javax.annotation.Nonnull

@ArtifactProviderFor(GriffonView)
class AppView extends AbstractJavaFXGriffonView {
    @MVCMember @Nonnull
    def builder
    @MVCMember @Nonnull
    def controller
    @MVCMember @Nonnull
    def model
	

    void initUI() {
		builder.with{
			//TO-DO
		}
    }
}